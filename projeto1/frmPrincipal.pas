unit frmPrincipal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, jpeg, ExtCtrls, Buttons;

type
  TfrmPricipal = class(TForm)
    MainMenu1: TMainMenu;
    Clientes1: TMenuItem;
    Fornecedores1: TMenuItem;
    Produtos1: TMenuItem;
    Consultas1: TMenuItem;
    Relatrios1: TMenuItem;
    EntradaProdutos1: TMenuItem;
    N1: TMenuItem;
    Sair1: TMenuItem;
    Clientes2: TMenuItem;
    Atendimento1: TMenuItem;
    Ajuda1: TMenuItem;
    Sobre1: TMenuItem;
    Funcionarios1: TMenuItem;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Panel3: TPanel;
    SpeedButton1: TSpeedButton;
    Image1: TImage;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    Timer1: TTimer;
    procedure Clientes2Click(Sender: TObject);
    procedure Fornecedores1Click(Sender: TObject);
    procedure Produtos1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Sobre1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPricipal: TfrmPricipal;

implementation

uses frmConsultas, frmClientes, frmFornecedores, frmProdutos, frmSobre;

{$R *.dfm}            

procedure TfrmPricipal.Clientes2Click(Sender: TObject);
begin
     formClientes.Show;
end;

procedure TfrmPricipal.Fornecedores1Click(Sender: TObject);
begin
    formFornecedores.Show;
end;

procedure TfrmPricipal.Produtos1Click(Sender: TObject);
begin
     formProdutos.Show;
end;

procedure TfrmPricipal.Sair1Click(Sender: TObject);
begin
     Close;
end;

procedure TfrmPricipal.Timer1Timer(Sender: TObject);
begin
     StatusBar1.Panels[1].Text:=  DatetoStr(date)+' : ' + TimetoStr(time);
end;

procedure TfrmPricipal.Sobre1Click(Sender: TObject);
begin
     formSobre.ShowModal;     
end;

end.
